package main

import (
	"os"

	"gitlab.com/vtvr/rtsp-simple-server/internal/core"
)

func main() {
	s, ok := core.New(os.Args[1:])
	if !ok {
		os.Exit(1)
	}
	s.Wait()
}
